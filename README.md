# Simple Snake

**Made with <3 by [stdmatt](http://stdmatt.com).**

<!--  -->
## Description:

<p align="center">
    <img src="./res/simple_snake.gif"/>
</p>

Because everyone needs to create a snake game at some point in life.

You can play it [here](https://stdmatt.com/deploy/games/simple_snake/index.html)

As usual, you are **very welcomed** to **share** and **hack** it.

<!--  -->
## Dependencies:

* [mcow_js_core](https://stdmatt.com/projects/libs/js/mcow_js_core.html)
* [mcow_js_canvas](https://stdmatt.com/projects/libs/js/mcow_js_canvas.html)

<!--  -->
## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

<!--  -->
## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
